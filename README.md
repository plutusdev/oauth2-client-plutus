# Plutus OAuth 2.0 Client

This package provides the Plutus Commerce [OAuth 2.0](http://oauth.net/2/) service provider & token for use with the [league/oauth2-client](https://oauth2-client.thephpleague.com/) package.

---

The OAuth 2.0 login flow, seen commonly around the web in the form of "Connect with Facebook/Google/etc." buttons, is a common integration added to web applications. The `league/oauth2-client` package provides a base for integrating with various OAuth 2.0 providers, without overburdening your application with the concerns of [RFC 6749](http://tools.ietf.org/html/rfc6749).

## Requirements

We support the following versions of PHP:

* PHP 8.0
* PHP 7.4
* PHP 7.3

## Install
```
composer require plutus/oauth2-client-plutus
```

## Example

## Authorization flow

```php
use League\OAuth2\Client\Provider\PlutusProvider;
use League\OAuth2\Client\Token\PlutusAccessToken;

$options = [
    'clientId'       => 'sso_client_id',    // The client ID assigned to you by the provider
    'clientSecret'   => 'sso_client_secret',   // The client password assigned to you by the provider
    'redirectUri'    => 'sso_redirect_url',
    'identityServer' => 'sso_public_url',
];

$this->provider = new PlutusProvider($options);

```

### Get authorization URL
```php
$authorizationUrl = $this->provider->getAuthorizationUrl();
```

### Get access token from authorization code
```php
$grant = new AuthorizationCode();
$accessToken = $this->provider->getAccessToken($grant, [
    'code' => $request->get('code'),
]);
```

### Get access token from refresh token
```php
$grant = new RefreshToken();

$accessToken = $this->provider->getAccessToken($grant, [
    'refresh_token' => $token->getRefreshToken(), // token is instance of PlutusAccessToken
]);
```

### Get user information

```php
$this->provider->getResourceOwner($accessToken) // instance of PlutusAccessToken
```

### Get JWKS

```php
$this->provider->getJwks()
```
