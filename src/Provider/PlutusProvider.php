<?php

namespace Plutus\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Grant\AbstractGrant;
use Plutus\OAuth2\Client\Token\PlutusAccessToken;
use InvalidArgumentException;

class PlutusProvider extends GenericProvider
{
    /**
     * @var string
     */
    protected $urlRevokeToken;

    /**
     * @var string
     */
    protected $urlGetJwks;

    /**
     * @var string
     */
    protected $logoutRedirectUri;
    /**
     * @var string
     */
    protected $urlLogout;

    /**
     * @param array $options
     * @param array $collaborators
     */
    public function __construct(array $options = [], array $collaborators = [])
    {
        $options = $this->prepareOptions($options);
        parent::__construct($options, $collaborators);
    }

    public function prepareOptions(array $options)
    {
        if (!isset($options['identityServer'])) {
            throw new InvalidArgumentException(
                'Required options not defined: ' . implode(', ', array_keys($missing))
            );
        }

        $identityServer = $options['identityServer'];
        unset($options['identityServer']);

        return array_merge([
            'urlAuthorize'            => "{$identityServer}/oauth2/auth",
            'urlLogout'               => "{$identityServer}/oauth2/sessions/logout",
            'urlAccessToken'          => "{$identityServer}/oauth2/token",
            'urlRevokeToken'          => "{$identityServer}/oauth2/revoke",
            'urlGetJwks'              => "{$identityServer}/.well-known/jwks.json",
            'urlResourceOwnerDetails' => "{$identityServer}/userinfo"
        ], $options);
    }

    protected function getUrlRevokeToken(): string
    {
        return $this->urlRevokeToken;
    }

    /**
     * Creates an access token from a response.
     *
     * The grant that was used to fetch the response can be used to provide
     * additional context.
     *
     * @param  array $response
     * @param  AbstractGrant $grant
     * @return AccessTokenInterface
     */
    protected function createAccessToken(array $response, AbstractGrant $grant)
    {
        return new PlutusAccessToken($response);
    }

    /**
     * Requests revoke an access token is set
     *
     * @param  PlutusAccessToken $token
     * @return AccessTokenInterface
     */
    public function revokeToken(PlutusAccessToken $token)
    {
        $params = [
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'token'         => $token->getToken(),
        ];

        $request  = $this->getRevokeTokenRequest($params, $token);
        $this->getResponse($request);

        return 'OK';
    }

    /**
     * Requests an access token using a specified grant and option set.
     *
     * @return JWKObject
     */
    public function getJwks()
    {
        $request = $this->getResolveJwksRequest();

        return $this->getParsedResponse($request);
    }

    /**
     * Requests an access token using a specified grant and option set.
     *
     * @return JWKObject
     */
    public function getLogoutUrl(PlutusAccessToken $token, array $options = [])
    {
        $base   = $this->urlLogout;
        $params = $this->getLogoutParameters($token, $options);
        $query  = $this->getAuthorizationQuery($params);

        return $this->appendQuery($base, $query);
    }

    protected function getLogoutParameters(PlutusAccessToken $token, array $options = [])
    {
        if (empty($options['state'])) {
            $options['state'] = $this->getRandomState();
        }

        if (!isset($options['post_logout_redirect_uri'])) {
            $options['post_logout_redirect_uri'] = $this->logoutRedirectUri;
        }

        $options['id_token_hint'] = $token->getIdToken();
        $options['client_id'] = $this->clientId;
        $options['client_secret'] = $this->clientSecret;
        $options['access_token'] = $token->getToken();

        return $options;
    }

    /**
     * Returns a prepared request for revoking an access token.
     *
     * @param array $params Query string parameters
     * @return RequestInterface
     */
    protected function getRevokeTokenRequest(array $params, PlutusAccessToken $token)
    {
        $method  = parent::METHOD_POST;
        $url     = $this->getUrlRevokeToken();
        $options = $this->optionProvider->getAccessTokenOptions($method, $params);

        return $this->createRequest($method, $url, $token, $options);
    }

    /**
     * Returns a prepared request for getting jwks
     *
     * @return RequestInterface
     */
    protected function getResolveJwksRequest()
    {
        $method = parent::METHOD_GET;
        $url    = $this->urlGetJwks;

        return $this->createRequest($method, $url, null, []);
    }
}
